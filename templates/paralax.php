<div class="paralax">
	<div class="container">
		<div class="paralaxa row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<h3> <strong>Zapraszamy</strong></br> do kontaktu i rezerwacji terminu</h3>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
			<?php
			$page_id = get_option('paralax_btn');
			if ( $page_id != '' ) :
			  $page_url = get_permalink( $page_id );?>
			  <a href="<?php echo $page_url; ?>" class="btn">Dowiedz się więcej</a>
			  <?php
			  endif
			  ?>
				<!-- <a class="btn">Dowiedz się więcej</a> -->
			</div>
		</div>
	</div>
</div>