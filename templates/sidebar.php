<?php if ( get_post_type( $post ) == 'method' ): ?>
  <?php dynamic_sidebar('sidebar-methods'); ?>
<?php elseif (is_page()) :  ?>
  <?php dynamic_sidebar('sidebar-page'); ?>

<?php else: ?>
  <?php dynamic_sidebar('sidebar-primary'); ?>
<?php endif ?>
