<article <?php post_class('row'); ?>>
<?php if (has_post_thumbnail( $post->ID )): ?>
  <div class="col-sm-6">
    <?php echo the_post_thumbnail('featured_post',array('class'=>'img-fluid') ); ?>
  </div>
  <div class="col-sm-6">
    <?php get_template_part( 'templates/featured', 'inner' ); ?>
  </div>
<?php else: ?>
	<div class="col-sm-12">
	<?php get_template_part( 'templates/featured', 'inner' ); ?>
	</div>
<?php endif ?>
</article>
