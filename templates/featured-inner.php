<header>
  <h4><a href="<?php echo get_the_permalink( $post ); ?>"><?php the_title(); ?></a></h4>
</header>
<div class="entry-content featured">
    <?php echo substr($post->post_content,0 , 200); ?>
</div>
<a href="<?php echo get_permalink( $post ); ?>" class="read-more button">czytaj dalej</a>