<?php use Roots\Sage\Titles;

if(!is_front_page()) : ?>
<hgroup class="page-header">
  <h1><?= Titles\title(); ?></h1>
  <?php echo Titles\get_page_subtitle($post->ID); ?>
</hgroup>
<?php endif ?>