<div class="opinion row">
  <div class="col-md-3 col-sm-12">
    <div class="green-block">
    <?php
    $green_block = get_option( 'green_block' );
    if ($green_block != '') :
    ?>
    <a class="green-href" href="<?php echo $green_block; ?>"> </a>
    <?php endif;?>
      <hgroup class="secondary-title">
        <h3>Pacjenci </br>o nas</h3>
        <h4>Dziękujemy</h4>
      </hgroup>
    </div>
</div>

<div class="col-md-8 push-md-1 col-sm-12">
  <div class="testimonials">
    <?php $testimonials = get_recent_testimonials(); foreach ($testimonials as $testimonial) : ?>
    <blockquote>
      <?php if (has_post_thumbnail($testimonial->ID )): ?>
        <figure><?php echo get_the_post_thumbnail( $testimonial->ID, 'contact_photo' ); ?></figure>
      <?php endif ?>
      <div class="cite">
        <div class="hide">
          <?php echo $testimonial->post_content ?>
        </div>
        <span class="expand"><?php echo __( 'expand', 'sage' ) ?></span>
        <cite>&dash; <?php echo $testimonial->post_title; ?></cite>
      </div>
    </blockquote>
  <?php endforeach; wp_reset_query(); ?>
</div>
</div>
</div>