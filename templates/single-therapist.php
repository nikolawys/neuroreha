<div class="row therapist">
  <div class="col-md-3 col-sm-12">
    <?php if (has_post_thumbnail( $therapist->ID )): ?>
      <figure><?php echo get_the_post_thumbnail($therapist->ID,'single_therapist' ); ?></figure>
    <?php endif ?>
    <h3><?php echo get_the_title( $therapist->ID ); ?></h3>
    <h5><?php echo get_post_meta( $therapist->ID, 'depcore_custom_title', true ); ?></h5>
  </div>
  <div class="col-md-9 col-sm-12">
    <?php echo $therapist->post_content; ?>

  </div>
</div>