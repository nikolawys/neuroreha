<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header class='main-title'>
      <h5 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <?php if (has_post_thumbnail( $post->ID )): ?>
      <figure class="post-thumbnail"><?php echo the_post_thumbnail('post_featured', array('class'=>'img-fluid post-featured')); ?></figure>
    <?php endif ?>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
