<footer class="content-info">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <a class="brand" href="<?= esc_url(home_url('/')); ?>"><!-- <?php bloginfo('name'); ?> -->
            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg">
          </a>
        </div>
        <div class="col-md-3 col-sm-12">
          <?php wp_nav_menu(array(
           'menu_class'      => 'footer-nav',
           'theme_location'  => 'footer_navigation',
           'menu_id'         => false,
           'depth'           => 2,
           ));
           ?>

         </div>
         <div class="col-md-3 col-sm-12">
           <h5>Skontaktuj się z nami</h5>
           <address>
            <h4>
              <?php $company_name = get_option( 'company_name' );
              echo $company_name ?>
            </h4>
            <?php
            $company_address = get_option( 'company_address' );
            echo str_replace("\n", "<br>", $company_address) ?>
          </address>
          <span>tel.: <a href="tel:+48<?php $company_number = get_option( 'company_number' );
        echo $company_number; ?>"><?php
        echo $company_number; ?></a></br>
            tel.: <a href="tel:+48<?php $sec_company_number = get_option( 'sec_company_number' );
    echo $sec_company_number; ?>"><?php
        echo $sec_company_number; ?></a></br>
            e-mail: <a href="mailto:<?php $company_mail = get_option( 'company_mail');
        echo $company_mail; ?>"> <?php
        echo $company_mail;?></a></span>
         </div>
         <?php dynamic_sidebar('sidebar-footer'); ?>
       </div>
     </div>
     <div class="copy-footer">
      <span>copyright &copy; 2017 <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?>
      </a></span>
    </div>
</footer>
