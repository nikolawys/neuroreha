<header class="banner">
  <div class="container">
    <div class="branding row">
      <div class="col-lg-3 col-md-12 col-sm-12">
        <a class="brand" href="<?php echo esc_url(home_url('/')); ?>"><!-- <?php bloginfo('name'); ?> --><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg" class='img-fluid'> </a>
      </div>
      <div class="header-info col-lg-8 push-1 col-md-12 col-sm-12">
      <div class="row">
        <div class="col-md-4 col-sm-12">
          <span class="desk"><a><img src="<?php echo get_template_directory_uri(); ?>/dist/images/pin.svg"><?php
            $company_address = get_option( 'company_address' );
            echo str_replace("\n", ", ", $company_address);
            ?></a>
          </span>
        </div>
        <div class="col-md-4 col-sm-12">
          <span class="mobil"><a href="tel:+48<?php $company_number = get_option( 'company_number' );echo $company_number; ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/phone.svg" ><?php echo $company_number; ?></a></span>
        </div>
        <div class="col-md-4 col-sm-12">
          <span class="desk"><a href="mailto:<?php $company_mail = get_option( 'company_mail');
            echo $company_mail;?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/email.svg" >
            <?php echo $company_mail;?></a></span>
        </div>

      </div>
            <span class="desk">
            <?php
              $fb_url = get_option( 'fb_funpage' );
              if ($fb_url != '') :?>
              <a href="<?php echo $fb_url ?>" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/dist/images/facebook.svg" ></a></span>
              <?php
              endif
              ?>
            </div>
          </div>
        </div>
        <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
          <div class="container">
           <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon"></span>
           </button>

           <?php
           wp_nav_menu([
             'menu'            => 'top',
             'theme_location'  => 'primary_navigation',
             'container'       => 'div',
             'container_id'    => 'bs4navbar',
             'container_class' => 'collapse navbar-collapse',
             'menu_id'         => false,
             'menu_class'      => 'navbar-nav mr-auto',
             'depth'           => 2,
             'fallback_cb'     => 'bs4navwalker::fallback',
             'walker'          => new bs4navwalker()
             ]);
             ?>
           </div>
         </nav>
       </header>
