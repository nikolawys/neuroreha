<?php
$args = array('post_type' => 'slider', 'post_status'=>'publish');
$slides = get_posts( $args );
?>
<div class='carousel'>
 <?php foreach ($slides as $slide): setup_postdata( $slide ); ?>
  <?php
    $thumb_id = get_post_thumbnail_id( $slide->ID );
    $thumb_url = wp_get_attachment_image_src($thumb_id,'slider_main', true);
    $thumb_title = get_the_title( $slide->ID );
   ?>
  <div style='background-image:url(<?php echo $thumb_url[0] ?>)'>
     <div class="description"><?php echo $thumb_title;
     the_content( ); ?></div>
  </div>
 <?php endforeach ?>
</div>
<?php wp_reset_query(); ?>