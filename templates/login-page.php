<?php 

function my_loginlogo() {

	$template_dir = get_template_directory_uri();
	echo "<style type='text/css'>

	body{
		background: #121212 url($template_dir/assets/images/core-light.svg) no-repeat;
		background-position: 50% 50%;
		overflow: hidden;
		}	

	.login h1 a{
		background-image: url($template_dir/assets/images/logo.svg);
		repeat: no-repeat;
		height: 230px;
		margin: 0 0 0 -454px;
		text-indent: -9999px;
		display: block;
		position: relative;
		float:left;
		display: block;
	}

	.login form{
		margin-top: 30px;
		background: none;
	}

	input[type=text]:focus, input[type=search]:focus, input[type=radio]:focus, input[type=tel]:focus, 
	input[type=time]:focus, input[type=url]:focus, input[type=week]:focus, input[type=password]:focus, 
	input[type=checkbox]:focus, input[type=color]:focus, input[type=date]:focus, input[type=datetime]:focus, 
	input[type=datetime-local]:focus, input[type=email]:focus, input[type=month]:focus, 
	input[type=number]:focus, select:focus, textarea:focus{
		border-color: grey;
		box-shadow: 0 0 2px grey;
	}

	input[type=checkbox]:checked:before{
		color: grey;
	}

	.wp-core-ui .button-primary{
		background: none;
		width: 100%;
		margin-top: 20px;
		text-shadow: none;
		box-shadow: none;
		border-color: #fcfdff;
		-webkit-box-shadow: none;
		-webkit-border-radius: 0;
		transition: all 0.5s ease 0s;
	}
	.login form .input, .login input[type=text]{
		font-size: 20px;
	}

	.wp-core-ui .button-group.button-large .button, .wp-core-ui .button.button-large{
		height: 35px;
		text-transform: uppercase;
	}

	.login label, .login #nav a, .login #backtoblog a{
		color: #fcfdff;
		line-height: 2em;
	}

	.login label, .login #nav a:hover, .login #backtoblog a:hover, .login #backtoblog a:focus, .login #nav a:focus, .login h1 a:focus{
		color: #cccccc;
		outline: none;
		border: none;
		box-shadow: none;
		-webkit-box-shadow: none;
	}
	.login #nav, .login #backtoblog{
		margin:0;
	}

	.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover, .wp-core-ui .button-primary:active, .wp-core-ui .button-primary:visited{
		background: #fcfdff;
		color: black;
		border-color: #cccccc;
		box-shadow: none;
	}
	.login #login_error, .login .message{
		border-left:4px solid #cccccc;
	}


	</style>";
}

add_action('login_head', 'my_loginlogo');


?>