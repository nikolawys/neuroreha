��    R      �  m   <      �     �               #     2     F     X     l     �     �     �     �  L   �            	   +     5     G  	   S     ]     i     z     �     �     �     �     �     �     �     	  
   	     	  
   !	     ,	     <	     J	     `	     e	     w	     �	     �	     �	     �	     �	     �	     
  	   -
     7
     E
     L
     [
     h
     w
     �
     �
     �
     �
     �
     �
     �
     �
       6   "     Y     `  ;   v     �     �     �     �  	   	       
   /     :     W  	   c     m     y     �  �   �     +  �  2     ,     F     Z     i     z     �     �     �  !   �               (  O   4     �     �  	   �     �     �     �     �     �       %        ?     Y     `     s     �     �     �     �     �     �     �     �     �                    4     Q  #   m     �     �     �  "   �               $     ,     ?     X     s     �     �     �     �     �     �     �          #  <   8     u     |  .   �  ,   �  
   �     �     
  	        !     9     I     e     t     �     �     �    �     �                     
          5   ;   +       ,       A         K   2   G   H                  >   P             I                  %           /      O   =         *   .      )       @                   $   J         :   F          L   3       <   M      "   4           ?   R              #           B   C             0   9   !   &      6      '   Q          -      7   D   8      E      (   N   	   1    &larr; Older comments Add New Method Add New News Add New Slider Add New Testimonial Add New Therapist Choose contact form Choose site to link Comments are closed. Company address Company e-mail Company name Company phone number (visible at the top). Please do not type prefix number. Contact details Contact form Continued Custom page title Edit Method Edit News Edit Slider Edit Testimonial Edit Therapist Error locating %s for inclusion Facebook funpage address Footer Footer Navigation Latest Posts Method MethodAdd New Method Methods New Method New News New Slider New Testimonial New Therapist Newer comments &rarr; News NewsAdd New News No Methods found No Methods found in Trash No News found No News found in Trash No Testimonials found No Testimonials found in Trash No Therapists found No Therapists found in Trash Not Found Page settings Pages: Parent Method: Parent News: Parent Slider: Parent Testimonial: Parent Therapist: Primary Primary Navigation Professional title Search Methods Search News Search Results for %s Search Testimonials Search Therapists Second phone number. Please do not type prefix number. Slider SliderAdd New Slider Sorry, but the page you were trying to view does not exist. Sorry, no results were found. Testimonial TestimonialAdd New Testimonial Testimonials Therapist TherapistAdd New Therapist Therapists Type company contact details View Method View News View Slider View Testimonial View Therapist You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience. expand Project-Id-Version: Neuroreha
POT-Creation-Date: 2017-05-29 13:00+0200
PO-Revision-Date: 2017-06-05 09:36+0200
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 &larr; Starsze komentarze Dodaj nową metodę Dodaj Artykuł Dodaj nowy Slajd Dodaj nowe Referencje Dodaj nowego terapeutę Wybierz formularz kontaktowy Wybierz strony do podlinkowania Dodawanie komentarzy wyłączone. Adres firmy Firmowy adres e-mail Nazwa firmy Numer telefonu firmy (widoczny u góry). Proszę nie wpisywać numeru prefiksu. Dane kontaktowe Formularz kontaktowy Kontynuuj Własny tytuł strony Edytuj metode Edytuj aktualności Edytuj pokaz Edytuj Opinię Edytuj terapeutę Błąd lokalizacji % s do włączenia Adres strony na Facebooku Stopka Nawigacja w Stopce Najnowsze Wpisy Metoda Dodaj nową metodę Metody Nowa metoda Nowe aktualności Nowy sliajd Nowa Opinia Nowy terapeuta Nowsze komentarze &rarr; Aktualności Dodaj Artykuł Nie znaleziono metod Nie znaleziono metod w koszu Nie znaleziono aktualności Nie znaleziono aktualności w koszu Nie znaleziono opinii Nie znaleziono opinii w Koszu Nie znaleziono terapeutów Nie znaleziono terapeutów w koszu Nie znaleziono Ustawienia strony Strony: Nadrzędna metoda. Aktualności nadrzędne: Nadrzędny pokaz slajdów. Nadrzędne wrażenie: Nadrzędny terapeuta: Główny Nawigacja główna Uzyskany tytuł zawodowy Wyszukaj metodę Przeszukaj Artykuły Wyniki wyszukiwania dla %s Szukaj Opinii Wyszukaj terapeutów Drugi numer telefonu. Proszę nie wpisywać numeru prefiksu. Slajdy Dodaj nowy Slajd Przepraszamy, ale szukana strona nie istnieje. Przepraszamy, brak rezultatów wyszukiwania. Referencja Dodaj nowe Referencje Rekomendacje Terapeuta Dodaj nowego terapeutę Fizykoterapeuci Informacje kontaktowe firmy Zobacz metodę Podgląd aktualności Pokaż suwak Zobacz opinię Zobacz terapeutę Używasz <strong>przestarzałej</strong> przeglądarki. Proszę o <a href=„http://browsehappy.com/„>uaktualnienie do innej przeglądarki</a> lub <a href=„http://www.google.com/chromeframe/?redirect=true”>zainstalować Google Chrome Frame</a> doświadczyć tej strony. rozwiń 