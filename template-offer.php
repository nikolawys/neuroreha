<?php
/**
 * Template Name: Oferta
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>
	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
<h4>Jak leczymy</h4>
<div class="offer content">

			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bobath.png" class='img-responsive'>
			<h2>Bobath</h2>
			<h4>Jak leczymy</h4>
			<p class="short">W ostatnich latach problem chorób naczyniowych mózgu staje się przedmiotem wielu badań, poszukiwań metod leczenia jak również zmniejszenie ryzyka ich występowania. Światowa Organizacja Zdrowia (WHO) już od 1974 roku prowadzi badania nad chorobami układu krążenia, a celem tych badań jest zwalczenie trzech najważniejszych czynników ryzyka: zwiększone stężenie cholesterolu we krwi, palenie tytoniu i nadciśnienie tętnicze.</p>
			<p class="cont">Można przytaczać wiele danych statystycznych oraz próbować porównywać, na którym miejscu pod względem występowania można umieścić udary. Jednak najważniejsze jest to, że choroby naczyniowe mózgu to duży problem społeczny i ekonomiczny. Polly Laidler podaje dane statystyczne opracowane w Stanach Zjednoczonych. Podaje się, ze w tym kraju 794 osoby na 100 tyś. Mieszkańców przechodzi ostry udar mózgowy, co sprawia, że społeczeństwo amerykańskie kosztuje to ponad 7 bilionów dolarów rocznie. W tę kwotę wlicza się koszty leczenia, koszty zakładów leczniczych oraz utratę miejsca pracy, spowodowaną inwalidztwem. Badania wykazują również, że pacjenci po udarze mózgu wymagają więcej krótkotrwałych pobytów w szpitalu niż to jest w przypadku innych jednostek chorobowych.</p>
			<p class="short">W Polsce pewnie nikt nie stara się prowadzić tak szerokich spekulacji i badań na temat strat poniesionych na wskutek chorób naczyniowych, ale przy obecnym stanie Służby Zdrowia w naszym kraju te straty muszą być ogromne.</p>
			<p class="cont">Wszystkie dokładne analizy wskazują, że problem leczenia, a zwłaszcza rehabilitacji powinien stać się kluczowym przedmiotem znalezienia sposobów opóźniania lub zapobiegania rozwojowi inwalidztwa poudarowego w naszym kraju. Musimy dążyć do poszukiwania metod rehabilitacji, które będą uwzględniać pielęgnację, opiekę prewencyjną, będą podnosić jakość życia pacjenta. Należy pamiętać, że udar mózgu to nie tylko zaburzenia ruchowe, ale również zaburzenia o charakterze poznawczym (percepcja, sensoryka), zaburzenia mowy, problemy z połykaniem. W wielu przypadkach stosując rehabilitację zapominamy o tych objawach. Polly Laidler bardzo odważnie pisze, że:</p>
			<blockquote>„większość przewlekłych niedołężności związanych z udarem jest nabyta w wyniku tradycyjnego postępowania i doktrynalnych technik (np. arbitralne przyjmowanie założeń teoretycznych, bez względu na ich wykonalność lub skutki)”. </blockquote>
</div>
