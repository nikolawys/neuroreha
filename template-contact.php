<?php
/**
 * Template Name: Strona kontaktu
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>
	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
<div class="content row hentry">
	<div class="col-md-4 col-sm-12">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/adres.png">
		<div class="address">
			<h5><?php $company_name = get_option( 'company_name' );
				echo $company_name ?></h5>
				<address><?php
					$company_address = get_option( 'company_address' );
					echo str_replace("\n", "<br>", $company_address)
					?>
				</address>
				<span>tel.: <a href="tel:+48<?php $company_number = get_option( 'company_number' );
					echo $company_number; ?>"><?php
					echo $company_number; ?></a></br>
					tel.: <a href="tel:+48<?php $sec_company_number = get_option( 'sec_company_number' );
					echo $sec_company_number; ?>"><?php
					echo $sec_company_number; ?></a></br>
					e-mail: <a href="mailto:<?php $company_mail = get_option( 'company_mail');
					echo $company_mail; ?>"> <?php
					echo $company_mail;?></a></span>
				</div>
				<div class='features reception'>
				  <?php dynamic_sidebar( 'sidebar-reception' ); ?>
				</div>

			</div>
			<div class="col-md-7 push-md-1 col-sm-12">
				<div id="map"></div>
			</div>

		</div>
		<h5>Formularz kontaktowy</h5>
		<h4 class="form7">Wypełnij i wyślij</h4>
<!-- 	<?php echo do_shortcode('[contact-form-7 id="17"]'); ?>
-->
<?php
$form = get_option( 'contact_form' );
if ( $form != '' ) echo do_shortcode( "[contact-form-7 id=$form]");
elseif ( is_user_logged_in() ) {
	$personalization = admin_url ( '/customize.php?return=%2Fventier%2Fwp-admin%2Fplugins.php%3Fplugin_status%3Dall%26paged%3D1%26s' );
	echo "Wybierz formularz w <a href='$personalization'>personalizacji</a> ";
}
?>
