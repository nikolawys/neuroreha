<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';

// Section: Name.
  $wp_customize->add_section( 'contact_form', array(
    'priority'       => 100,
    'title'          => __( 'Contact form', 'neuroreha' ),
    'description'    => __( 'Choose contact form', 'neuroreha' ),
    'capability'     => 'edit_theme_options',
    ) );
// Setting: Name.
  $wp_customize->add_setting( 'contact_form', array(
    'type'                 => 'option',
    'transport'            => 'refresh',
    'capability'           => 'edit_theme_options',
    ) );
  require_once  'contact_form_7.php';
  $wp_customize->add_control( new \Contact_form_7_Dropdown_Custom_Control( $wp_customize, 'contact_form', array(
    'label'   => 'Contact Form',
    'section' => 'contact_form',
    'settings'   => 'contact_form',
    'priority' => 2
    ) ) );
  $wp_customize->add_section( 'contact_details', array(
   'priority'       => 80,
   'title'          => __( 'Contact details', 'neuroreha' ),
   'description'    => __( 'Type company contact details', 'neuroreha' ),
   'capability'     => 'edit_theme_options',
   ) );
 // Setting: Name.
  $wp_customize->add_setting( 'company_name', array(
   'type'                 => 'option',
   'default'              => 'Neuroreha Sp. z o.o.',
   'transport'            => 'refresh', // Options: refresh or postMessage.
   'sanitize_callback'    => 'wp_strip_all_tags',
   ) );

 // Control: Name.
  $wp_customize->add_control( 'company_name', array(
   'label'       => __( 'Company name', 'neuroreha' ),
   'section'     => 'contact_details',
   'type'        => 'text',
   'settings'    => 'company_name',
   ) );

 // Setting: Contact.
  $wp_customize->add_setting( 'company_address', array(
   'type'                 => 'option',
   'default'              => "ul. Samborska 15\n45-316 Opole",
   'transport'            => 'refresh', // Options: refresh or postMessage.
   'sanitize_callback'    => 'esc_textarea',
   ) );

  $wp_customize->add_control( 'company_address', array(
   'label'       => __( 'Company address', 'neuroreha' ),
   'section'     => 'contact_details',
   'type'        => 'textarea',
   'settings'    => 'company_address',
   ) );

  $wp_customize->add_setting( 'company_number', array(
   'type'                 => 'option',
   'default'              => '77 54 45 833',
   'transport'            => 'refresh', // Options: refresh or postMessage.
   'sanitize_callback'    => 'wp_strip_all_tags',
   ) );
 // Control: Name.
  $wp_customize->add_control( 'company_number', array(
   'label'       => __( 'Company phone number (visible at the top). Please do not type prefix number.', 'neuroreha' ),
   'section'     => 'contact_details',
   'type'        => 'text',
   'settings'    => 'company_number',
   ) );
  $wp_customize->add_setting( 'sec_company_number', array(
   'type'                 => 'option',
   'default'              => '784 400 421',
   'transport'            => 'refresh', // Options: refresh or postMessage.
   'sanitize_callback'    => 'wp_strip_all_tags',
   ) );
 // Control: Name.
  $wp_customize->add_control( 'sec_company_number', array(
   'label'       => __( 'Second phone number. Please do not type prefix number.', 'neuroreha' ),
   'section'     => 'contact_details',
   'type'        => 'text',
   'settings'    => 'sec_company_number',
   ) );
  $wp_customize->add_setting( 'company_mail', array(
   'type'                 => 'option',
   'default'              => 'info@neuroreha.pl',
   'transport'            => 'refresh',
   'sanitize_callback'    => 'wp_strip_all_tags',
   ) );
 // Control: Name.
  $wp_customize->add_control( 'company_mail', array(
   'label'       => __( 'Company e-mail', 'neuroreha' ),
   'section'     => 'contact_details',
   'type'        => 'text',
   'settings'    => 'company_mail',
   ) );
 // Setting: Name.
  $wp_customize->add_setting( 'fb_funpage', array(
   'type'                 => 'option',
   // 'default'              => 'default',
   'transport'            => 'refresh', // Options: refresh or postMessage.
   ) );
 // Control: Name.
  $wp_customize->add_control( 'fb_funpage', array(
   'label'       => __( 'Facebook funpage address', 'neuroreha' ),
   'section'     => 'contact_details',
   'type'        => 'text',
   'settings'    => 'fb_funpage',
   ) );


  $wp_customize->add_setting( 'paralax_btn', array(
   'type'                 => 'option',
   'transport'            => 'refresh',
   'capability'           => 'edit_theme_options',
   ) );
 // Control: Name.
  require_once 'pages.php';
  $wp_customize->add_control( new \Pages_Dropdown_Custom_Control ( $wp_customize,'paralax_btn', array(
   'label'       => __( 'Choose site to link - paralax', 'neuroreha' ),
   'section'     => 'contact_details',
   'settings'    => 'paralax_btn',
   ) ) );
  $wp_customize->add_section( 'blocks', array(
   'priority'       => 50,
   'title'          => __( 'Main page blocks', 'neuroreha' ),
   'description'    => __( 'Choose site', 'neuroreha' ),
   'capability'     => 'edit_theme_options',
   ) );
  $wp_customize->add_setting( 'green_block', array(
   'type'           => 'option',
   'transport'      => 'refresh',
   'capability'     => 'edit_theme_options',
   'sanitize_callback' => 'esc_url',
   ));
  $wp_customize->add_control( 'green_block', array(
    'label'         => __( 'Pacjenci o nas - wybierz stronę do linkowania', 'neuroreha'),
    'section'       => 'blocks',
    'type'          => 'text',
    'settings'      => 'green_block',
   ) ) ;
}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');

