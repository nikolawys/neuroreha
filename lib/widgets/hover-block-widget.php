<?php
/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
 */


class Hover_Block_Widget extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     **/
    function __construct() {
        $widget_ops = array( 'classname' => 'hover_block_widget', 'description' => 'Blok z linkiem i tłem' );
        parent::__construct( 'hover_block_widget', 'Hover Block', $widget_ops );
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    function widget( $args, $instance ) {
      extract( $args, EXTR_SKIP );
        echo $before_widget;
       ?>
              <div class="therapy">
        <a href="<?php echo $instance['url'] ?>">
        <img src="<?php echo $instance['image'] ?>">
        <br>
        <span><?php echo $instance['title'] ?></span></a>
        <p>
           <?php echo $instance['content'] ?>
        </p>
      </div>

      <?php
      echo $after_widget;
    }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    function update( $new_instance, $old_instance ) {

        $instance = $old_instance;

        $instance['title'] = strip_tags($new_instance['title']);
        $instance['image'] = $new_instance['image'];
        $instance['url'] = $new_instance['url'];
        $instance['content'] = ($new_instance['content']);

        return $instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void Echoes it's output
     **/
    function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, array(  ) );
        $title = $url = $content = '';
        if (!empty($instance)) {
            $title = $instance['title'];
            $url = $instance['url'];
            $image = $instance['image'];
            $content = $instance['content'];
        }

        ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'neuroreha'); ?>:</label>
          <input id="<?php echo $this->get_field_id('title'); ?>" class="widefat" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('url'); ?>"><?php _e('Url', 'neuroreha'); ?>:</label>
          <input id="<?php echo $this->get_field_id('url'); ?>" class="widefat" type="url" name="<?php echo $this->get_field_name('url'); ?>" value="<?php echo $url; ?>" />
        </p>
        <p>
          <label for="cc-image-upload-file"><?php _e('Image', 'neuroreha'); ?>:</label><br>
          <label for="cc-image-upload-file">
            <input type="url" id="<?php echo $this->get_field_id('image') ?>" class="widefat custom_media_image" name="<?php echo $this->get_field_name('image'); ?>" value="<?php echo $image; ?>" />
            <input type="button" id="<?php echo $this->get_field_id('image') ?>-button" class="button media-upload-button" value="Upload file" />
            <label for="cc-image-upload-file"><span class="description"><?php _e( 'Enter URL or upload file', 'neuroreha' ); ?></span></label>
          </label>
        </p>
        <p>
            <label for="text-input"><?php echo __('Content','sage') ?></label><br>
            <textarea name="<?php echo $this->get_field_name('content'); ?>" id="content" cols="30" rows="10"><?php echo $content ?></textarea>
        </p>

        <?php
    }
}

add_action( 'widgets_init', create_function( '', "register_widget( 'Hover_Block_Widget' );" ) );