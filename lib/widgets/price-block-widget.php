<?php
/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
 */

class Price_Block_Widget extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     **/
    function __construct() {
      $widget_ops = array( 'classname' => 'price_block_widget', 'description' => 'Blok z ceną' );
      parent::__construct( 'price_block_widget', 'Price Block', $widget_ops );
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    function widget( $args, $instance ) {
      extract( $args, EXTR_SKIP );
      echo $before_widget;
      ?>
      <div class="price">
        <span><?php echo $instance['price'] ?> zł</span>
        <h5>
         <?php echo $instance['title'] ?> 
       </h5>
       <p class="description">   
        <?php echo $instance['content'] ?> 
      </p>
    </div>

    <?php
    echo $after_widget;
  }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    function update( $new_instance, $old_instance ) {

      $instance = $old_instance;

      $instance['price'] = $new_instance['price'];
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['content'] = ($new_instance['content']);

      return $instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void Echoes it's output
     **/
    function form( $instance ) {
      $instance = wp_parse_args( (array) $instance, array(  ) );
      $title = $price = $content = '';
      if (!empty($instance)) {
        $title = $instance['title'];
        $price = $instance['price'];
        $content = $instance['content'];
      }

      ?>
      <p>
        <label for="<?php echo $this->get_field_id('price'); ?>"><?php _e('Price', 'neuroreha'); ?></label>
        <input id="<?php echo $this->get_field_id('price'); ?>" class="widefat" type="url" name="<?php echo $this->get_field_name('price'); ?>" value="<?php echo $price; ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'neuroreha'); ?>:</label>
        <input id="<?php echo $this->get_field_id('title'); ?>" class="widefat" type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" />
      </p>
      <p>
        <label for="text-input"><?php echo __('Content','sage') ?></label><br>
        <textarea name="<?php echo $this->get_field_name('content'); ?>" id="content" cols="30" rows="10"><?php echo $content ?></textarea>
      </p>

      <?php
    }
  }

  add_action( 'widgets_init', create_function( '', "register_widget( 'Price_Block_Widget' );" ) );