<?php
/**
 * new WordPress Widget format
 * Wordpress 2.8 and above
 * @see http://codex.wordpress.org/Widgets_API#Developing_Widgets
*/

class Reception_Block_Widget extends WP_Widget {

    /**
     * Constructor
     *
     * @return void
     **/
    function __construct() {
      $widget_ops = array( 'classname' => 'reception_block_widget', 'description' => 'Blok z danymi recepcji' );
      parent::__construct( 'reception_block_widget', 'Reception Block', $widget_ops );
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    function widget( $args, $instance ) {
      extract( $args, EXTR_SKIP );
      echo $before_widget;
      ?>
        <div class="col-md-3 col-sm-12">
          <img src="<?php echo $instance['image'] ?>" class='img-fluid'>
        </div>
        <div class="col-md-8 col-sm-12 col-md-push-1">
        <span>      
        <?php echo $before_title; 
        echo $instance['reception']; 
        echo $after_title; ?>
        <a href="tel:<?php echo $instance['number'] ?> ">
         <?php echo $instance['number'] ?> 
       </a></span>
          
        </div>
    <?php
    echo $after_widget;
  }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    function update( $new_instance, $old_instance ) {

      $instance = $old_instance;

      $instance['image'] = $new_instance['image'];
      $instance['reception'] = $new_instance['reception'];
      $instance['number'] = ($new_instance['number']);

      return $instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void Echoes it's output
     **/
    function form( $instance ) {
      $instance = wp_parse_args( (array) $instance, array(  ) );
      $number = $reception = $number = '';
      if (!empty($instance)) {
        $image = $instance['image'];
        $number = $instance['number'];
        $reception = $instance['reception'];
      }

      ?>
      <p>
        <label for="cc-image-upload-file"><?php _e('Image', 'neuroreha'); ?>:</label><br>
        <label for="cc-image-upload-file">
          <input type="url" id="<?php echo $this->get_field_id('image') ?>" class="widefat custom_media_image" name="<?php echo $this->get_field_name('image'); ?>" value="<?php echo $image; ?>" />
          <input type="button" id="<?php echo $this->get_field_id('image') ?>-button" class="button media-upload-button" value="Upload file" />
          <label for="cc-image-upload-file"><span class="description"><?php _e( 'Enter URL or upload file', 'neuroreha' ); ?></span></label>
        </label>        
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('reception'); ?>"><?php _e('Receptionist', 'neuroreha'); ?></label>
        <input id="<?php echo $this->get_field_id('reception'); ?>" class="widefat" type="url" name="<?php echo $this->get_field_name('reception'); ?>" value="<?php echo $reception; ?>" />
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Phone number', 'neuroreha'); ?>:</label>
        <input id="<?php echo $this->get_field_id('number'); ?>" class="widefat" type="text" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo $number; ?>" />
      </p>
      <p>

      </p>
    
      <?php
    }
  }

  add_action( 'widgets_init', create_function( '', "register_widget( 'Reception_Block_Widget' );" ) );