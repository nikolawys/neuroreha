<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;
$post_types = array('methods','slider','terapist','testimonial' ,'news');

foreach ($post_types as $name) {
  require_once "post-types/{$name}-cpt.php";

}

function custom_excerpt_length( $length ) {
  return 20;
}
add_filter( 'excerpt_length', __NAMESPACE__ . '\\custom_excerpt_length', 999 );


function new_excerpt_more($more) {
   global $post;
   return ' ';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\new_excerpt_more');

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage'),
    'footer_navigation' => __('Footer Navigation', 'sage')
  ]);

  add_image_size( 'slider_main', 1600, 400, true );
  add_image_size( 'featured_post', 350, 135, true );
  add_image_size( 'post_featured', 825, 400, true );
  add_image_size( 'single_therapist', 250, 205, true );
  add_image_size( 'logo_carousel', 99999, 70, false );
  add_image_size( 'contact_photo', 150, 150, true );


  add_theme_support('post-thumbnails');

  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Methods', 'sage'),
    'id'            => 'sidebar-methods',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Pages', 'sage'),
    'id'            => 'sidebar-page',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Home top', 'sage'),
    'id'            => 'sidebar-home-top',
    'before_widget' => '<section class="widget %1$s %2$s col-md-4 col-sm-12">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Home bottom', 'sage'),
    'id'            => 'sidebar-home-bottom',
    'before_widget' => '<section class="widget %1$s %2$s  col-md-4 col-sm-12">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Courses', 'sage'),
    'id'            => 'sidebar-courses',
    'before_widget' => '<section class="widget %1$s %2$s  col-md-4 col-sm-12">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
  register_sidebar([
    'name'          => __('Price list', 'sage'),
    'id'            => 'sidebar-prices',
    'before_widget' => '<section class="widget %1$s %2$s  col-md-3 col-sm-12">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ]);
  register_sidebar([
    'name'          => __('Reception contact information', 'sage'),
    'id'            => 'sidebar-reception',
    'before_widget' => '<section class="widget %1$s %2$s  row reception">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4>',
    'after_title'   => '</h4>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-contact.php'),
    is_page_template('template-methods.php'),
    is_page_template('template-about.php'),
    is_page_template('template-custom.php'),
    is_page_template('template-courses.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  wp_enqueue_script( 'gmap', '//maps.google.com/maps/api/js?key=AIzaSyASFnyMY8H5smlG_5baromx-5HaWF046Ws');
  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


add_filter('get_image_tag_class', function($class){
   $class .= ' img-fluid';
    return $class;
} );
