<?php

add_action( 'init', 'register_cpt_news' );

function register_cpt_news() {

  $labels = array(
    'name'               => __( 'News', 'sage' ),
    'singular_name'      => __( 'News', 'sage' ),
    'add_new'            => _x( 'Add New News', 'News', 'sage' ),
    'add_new_item'       => __( 'Add New News', 'sage' ),
    'edit_item'          => __( 'Edit News', 'sage' ),
    'new_item'           => __( 'New News', 'sage' ),
    'view_item'          => __( 'View News', 'sage' ),
    'search_items'       => __( 'Search News', 'sage' ),
    'not_found'          => __( 'No News found', 'sage' ),
    'not_found_in_trash' => __( 'No News found in Trash', 'sage' ),
    'parent_item_colon'  => __( 'Parent News:', 'sage' ),
    'menu_name'          => __( 'News', 'sage' ),
    );

  $args = array(
    'labels'              => $labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 2,
    'menu_icon'         => 'dashicons-pressthis',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => true,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'supports'            => array('title', 'editor', 'author', 'thumbnail',),
    );

    register_post_type( 'news', $args );

}
