<?php


add_action( 'init', 'register_cpt_therapist' );

function register_cpt_therapist() {

    $labels = array(
    'name'               => __( 'Therapists', 'sage' ),
    'singular_name'      => __( 'Therapist', 'sage' ),
    'add_new'            => _x( 'Add New Therapist', 'Therapist', 'sage' ),
    'add_new_item'       => __( 'Add New Therapist', 'sage' ),
    'edit_item'          => __( 'Edit Therapist', 'sage' ),
    'new_item'           => __( 'New Therapist', 'sage' ),
    'view_item'          => __( 'View Therapist', 'sage' ),
    'search_items'       => __( 'Search Therapists', 'sage' ),
    'not_found'          => __( 'No Therapists found', 'sage' ),
    'not_found_in_trash' => __( 'No Therapists found in Trash', 'sage' ),
    'parent_item_colon'  => __( 'Parent Therapist:', 'sage' ),
    'menu_name'          => __( 'Therapists', 'sage' ),
    );

    $args = array(
    'labels'              => $labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'         => 'dashicons-smiley',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => false,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => true,
    'capability_type'     => 'page',
    'supports'            => array(
                  'title', 'editor',  'thumbnail','page-attributes',),
    );

    register_post_type( 'therapist', $args );

}


function show_therapists_( $atts ) {
  $atts = shortcode_atts( array(
    'id' => null
    ), $atts );

  $post_args = array(
    'post_type' => 'therapist',
    'post_status'=>'publish',
    'limit' => -1,
    'posts_per_page' => -1,
    'order_by' => 'menu_order ID',
    'order' => "ASC"
    );

  $therapists = get_posts( $post_args );
  foreach ($therapists as $therapist) {
    setup_postdata( $therapist );
    include get_template_directory() . '/templates/single-therapist.php';
  }
  wp_reset_query();

}
add_shortcode( 'terapeuci','show_therapists_' );

/**
 * Register a meta box using a class.
 */
class DEPCORE_Therapist_Meta_Box {

    /**
     * Constructor.
     */
    public function __construct() {
        if ( is_admin() ) {
            add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
            add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
        }

    }

    /**
     * Meta box initialization.
     */
    public function init_metabox() {
        add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
        add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
    }

    /**
     * Adds the meta box.
     */
    public function add_metabox() {
        add_meta_box(
            'page-settings-metabox',
            __( 'Page settings', 'textdomain' ),
            array( $this, 'render_metabox' ),
            'therapist',
            'advanced',
            'default'
        );

    }

    /**
     * Renders the meta box.
     */
    public function render_metabox( $post ) {
        // Add nonce for security and authentication.
        wp_nonce_field( 'custom_nonce_action', 'custom_nonce' );
        $old_value = get_post_meta( $post->ID, 'depcore_custom_title', true );

        ?>
          <label for=""><?php echo __( 'Professional title', 'sage' ) ?> </label>
          <input type="text" name='depcore_custom_title' value='<?php echo $old_value ?>'>
        <?php

    }

    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post ) {
        // Add nonce for security and authentication.
        $nonce_name   = isset( $_POST['custom_nonce'] ) ? $_POST['custom_nonce'] : '';
        $nonce_action = 'custom_nonce_action';

        // Check if nonce is set.
        if ( ! isset( $nonce_name ) )  return;

        // Check if nonce is valid.
        if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) ) return;

        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) return;

        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) return;

        // Check if not a revision.
        if ( wp_is_post_revision( $post_id ) ) return;

        if (isset($_POST['depcore_custom_title'])) {
          update_post_meta( $post_id, 'depcore_custom_title', $_POST['depcore_custom_title'] );
        }
    }
}

new DEPCORE_Therapist_Meta_Box();