<?php
add_action( 'init', 'register_cpt_slider' );

function register_cpt_slider() {

    $labels = array(
    'name'               => __( 'Slider', 'sage' ),
    'singular_name'      => __( 'Slider', 'sage' ),
    'add_new'            => _x( 'Add New Slider', 'Slider', 'sage' ),
    'add_new_item'       => __( 'Add New Slider', 'sage' ),
    'edit_item'          => __( 'Edit Slider', 'sage' ),
    'new_item'           => __( 'New Slider', 'sage' ),
    'view_item'          => __( 'View Slider', 'sage' ),
    'search_items'       => __( 'Search Slider', 'sage' ),
    'not_found'          => __( 'No Slider found', 'sage' ),
    'not_found_in_trash' => __( 'No Slider found in Trash', 'sage' ),
    'parent_item_colon'  => __( 'Parent Slider:', 'sage' ),
    'menu_name'          => __( 'Slider', 'sage' ),
    );

    $args = array(
    'labels'              => $labels,
    'hierarchical'        => false,
    // 'description'         => 'description',
    // 'taxonomies'          => array( 'category' ),
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'         => 'dashicons-images-alt',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => true,
    'has_archive'         => false,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => true,
    'capability_type'     => 'page',
    'supports'            => array( 'title', 'editor', 'thumbnail',),
    );

    register_post_type( 'slider', $args );

}
