<?php

add_action( 'init', 'register_cpt_testimonial' );

function register_cpt_testimonial() {

    $labels = array(
    'name'               => __( 'Testimonials', 'sage' ),
    'singular_name'      => __( 'Testimonial', 'sage' ),
    'add_new'            => _x( 'Add New Testimonial', 'Testimonial', 'sage' ),
    'add_new_item'       => __( 'Add New Testimonial', 'sage' ),
    'edit_item'          => __( 'Edit Testimonial', 'sage' ),
    'new_item'           => __( 'New Testimonial', 'sage' ),
    'view_item'          => __( 'View Testimonial', 'sage' ),
    'search_items'       => __( 'Search Testimonials', 'sage' ),
    'not_found'          => __( 'No Testimonials found', 'sage' ),
    'not_found_in_trash' => __( 'No Testimonials found in Trash', 'sage' ),
    'parent_item_colon'  => __( 'Parent Testimonial:', 'sage' ),
    'menu_name'          => __( 'Testimonials', 'sage' ),
    );

    $args = array(
    'labels'              => $labels,
    'hierarchical'        => true,
    'description'         => 'description',
    'taxonomies'          => array( 'category' ),
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'         => 'dashicons-format-status',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => true,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => true,
    'capability_type'     => 'post',
    'supports'            => array(
                  'title', 'editor', 'thumbnail',
                ),
    );

    register_post_type( 'testimonial', $args );

}


function get_recent_testimonials(){

  $args = array('post_type' => 'testimonial','limit' => 3,'post_status'=>'publish');

  $testimonials = get_posts( $args );

  return $testimonials;

}