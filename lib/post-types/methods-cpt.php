<?php

add_action( 'init', 'register_cpt_method' );

function register_cpt_method() {

    $labels = array(
    'name'               => __( 'Methods', 'sage' ),
    'singular_name'      => __( 'Method', 'sage' ),
    'add_new'            => _x( 'Add New Method', 'Method', 'sage' ),
    'add_new_item'       => __( 'Add New Method', 'sage' ),
    'edit_item'          => __( 'Edit Method', 'sage' ),
    'new_item'           => __( 'New Method', 'sage' ),
    'view_item'          => __( 'View Method', 'sage' ),
    'search_items'       => __( 'Search Methods', 'sage' ),
    'not_found'          => __( 'No Methods found', 'sage' ),
    'not_found_in_trash' => __( 'No Methods found in Trash', 'sage' ),
    'parent_item_colon'  => __( 'Parent Method:', 'sage' ),
    'menu_name'          => __( 'Methods', 'sage' ),
    );

    $args = array(
    'labels'              => $labels,
    'hierarchical'        => true,
    // 'description'         => 'description',
    // 'taxonomies'          => array( 'category' ),
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 5,
    'menu_icon'         => 'dashicons-image-filter',
    'show_in_nav_menus'   => true,
    'publicly_queryable'  => true,
    'exclude_from_search' => false,
    'has_archive'         => true,
    'query_var'           => true,
    'can_export'          => true,
    'rewrite'             => true,
    'capability_type'     => 'page',
    'supports'            => array(
                  'title', 'editor', 'author', 'thumbnail',
                ),
    );

    register_post_type( 'method', $args );

}
