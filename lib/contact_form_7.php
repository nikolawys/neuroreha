<?php

if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;

/**
 * Class to create a custom post control
 */
class Contact_form_7_Dropdown_Custom_Control extends WP_Customize_Control
{
    private $forms = false;

    public function __construct($manager, $id, $args = array(), $options = array()){
        $postargs = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);

        $this->forms = get_posts($postargs);

        parent::__construct( $manager, $id, $args );
    }

    /**
    * Render the content on the theme customizer page
    */
    public function render_content()
    {
        if(!empty($this->forms)){
            ?>
              <label>
                  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                  <select data-customize-setting-link="<?php echo $this->id ?>" name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
                  <option value="">--Wybierz formularz--</option>
                  <?php
                      foreach ( $this->forms as $form )
                        printf('<option value="%s" %s>%s</option>', $form->ID, selected($this->value(), $form->ID, false), $form->post_title);
                  ?>
                  </select>
              </label>
          <?php
        }
          else {
            $url = admin_url('/plugin-install.php?s=contact+form+7&tab=search&type=term');
            echo "Brak potrzebnej wtyczki Contact Form 7 - zainstaluj <a href='$url'>tutaj</a>";
        }
    }
}
?>

