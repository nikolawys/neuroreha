<?php
 

function load_custom_wp_admin_style() {
  wp_enqueue_media();
  wp_enqueue_script( 'widget-media-upload', get_template_directory_uri().'/lib/widgets/assets/media_upload.js', array('jquery') );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


include 'widgets/hover-block-widget.php';
include 'widgets/price-block-widget.php';
include 'widgets/reception-block-widget.php';