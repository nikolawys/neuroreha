<?php

namespace Roots\Sage\Titles;

/**
 * Page titles
 */
function title() {
  if (is_home()) {
    if (get_option('page_for_posts', true)) {
      return get_the_title(get_option('page_for_posts', true));
    } else {
      return __('Latest Posts', 'sage');
    }
  } elseif (is_archive()) {
    return str_replace('Archiwa: ', '',get_the_archive_title());
  } elseif (is_search()) {
    return sprintf(__('Search Results for %s', 'sage'), get_search_query());
  } elseif (is_404()) {
    return __('Not Found', 'sage');
  } else {
    return get_the_title();
  }
}

function get_page_subtitle($post_id){
  $sub = get_post_meta( $post_id, 'depcore_custom_title', true );
  if ( $sub != '' ) return "<h4>$sub</h4>";
  else return "<h4 class=no-space></h4>";
}

