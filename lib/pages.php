<?php
if ( ! class_exists( 'WP_Customize_Control' ) )
    return NULL;

/**
 * Class to create a custom post control
 */
class Pages_Dropdown_Custom_Control extends WP_Customize_Control
{
    private $pages = false;

    public function __construct($manager, $id, $args = array(), $options = array()){
        $postargs = array('post_type' => 'page', 'posts_per_page' => -1,'post_status'=>'publish');

        $this->pages = get_posts($postargs);

        parent::__construct( $manager, $id, $args );
    }

    /**
    * Render the content on the theme customizer page
    */
    public function render_content()
    {
        if(!empty($this->pages)){
            ?>
              <label>
                  <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                  <select data-customize-setting-link="<?php echo $this->id ?>" name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>">
                  <option value="">--Choose page--</option>
                  <?php
                      foreach ( $this->pages as $page )
                        printf('<option value="%s" %s>%s</option>', $page->ID, selected($this->value(), $page->ID, false), $page->post_title);
                  ?>
                  </select>
              </label>
          <?php
        }
    }
}
?>


