<?php
/**
 * Template Name: Metody
 */
?>

<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('templates/page', 'header'); ?>
	<?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
<h4>Jak leczymy</h4>
<div class="post content">
	<div class="post row">
		<div class="col-md-5 col-sm-12">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/post.png">
		</div>
		<div class="col-md-7 col-sm-12">
			<h2>Bobath</h2>
			<p class="short">Koncepcja rehabilitacji dla pacjentów po przebytym udarze mózgu lub innych urazach mózgowo czaszkowych.</p>
			<p class="cont">Udar mózgu - pojęcie ogólne ( kliniczne) określające każde nagłe wystąpienie mózgowych objawów ogniskowych wywołanych niedostatecznym krążeniem mózgowym powstałym z przyczyn miejscowych lub ogólnych.</p>
			<a class="cont-btn">Dowiedz się więcej</a>
		</div>
	</div>
	<div class="post row">
		<div class="col-md-5 col-sm-12">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/post.png">
		</div>
		<div class="col-md-7 col-sm-12">
			<h2>PNF</h2>
			<p class="short">Metoda PNF została opracowana przez Hermana Kabata, a następnie rozwinięta przy współpracy z Margaret Knott i Dorothy Voss.</p>
			<p class="cont">Jest to sposób leczenia dysfunkcji nerwowo-mięśniowych, objawiających się zaburzeniami pracy na obwodzie (spastyczność, sztywność, wiotkość, zaburzenia koordynacji i siły) za pomocą torowania, ułatwiania przepływu informacji - głownie poprzez pobudzenie proprioreceptorów (receptorów narządu ruchu).</p>
			<a class="cont-btn">Dowiedz się więcej</a>
		</div>
	</div>
	<div class="post row">
		<div class="col-md-5 col-sm-12">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/post.png">
		</div>
		<div class="col-md-7 col-sm-12">
			<h2>Bobath</h2>
			<p class="short">Koncepcja rehabilitacji dla pacjentów po przebytym udarze mózgu lub innych urazach mózgowo czaszkowych.</p>
			<p class="cont">Udar mózgu - pojęcie ogólne ( kliniczne) określające każde nagłe wystąpienie mózgowych objawów ogniskowych wywołanych niedostatecznym krążeniem mózgowym powstałym z przyczyn miejscowych lub ogólnych.</p>
			<a class="cont-btn">Dowiedz się więcej</a>
		</div>
	</div>
</div>
