<?php
/**
 * Template Name: Strona główna
 */
  $args = array('post_type' => 'post','post_status'=>'publish','limit'=> '1');
  $recent = get_posts( $args );
  wp_reset_query();

  $args = array('post_type' => 'news','post_status'=>'publish','limit'=> '1');
  $news = get_posts( $args );
  wp_reset_query();

?>
<?php get_template_part('templates/page', 'header'); ?>
</div>
</div>

<?php get_template_part('templates/slider'); ?>
<div class="container">
    <?php get_template_part( 'templates/testimonials', 'carousel' ); ?>
    <div class="row">
      <div class="col-md-12">
        <hgroup class="main-title">
          <h2>Oferta<br> Neuroreha</h2>
          <h4>Co oferujemy</h4>
        </hgroup>
      </div>
    </div>
    <div class='features row'>
      <?php dynamic_sidebar( 'sidebar-home-top' ); ?>
    </div>
    
    <div class="row">
      <div class="col-md-12">
        <hgroup class="main-title">
          <h2>Dlaczego warto wybrać<br> Neuroreha</h2>
          <h4>Co oferujemy</h4>
        </hgroup>
      </div>
    </div>
    <div class='features bottom row'>
      <?php dynamic_sidebar( 'sidebar-home-bottom' ); ?>
    </div>
</div>
<?php get_template_part('templates/paralax'); ?>
<div class="container">
  <div class="row">
    <div class="col-md-6 col-sm-12">
      <hgroup class="main-title">
        <h2>Artykuły </br> neuroreha</h2>
        <h4>Nasz blog</h4>
      </hgroup>
      <?php
        $post= $recent[0];
        get_template_part( 'templates/content', 'featured' );
        ?>
    </div>
    <?php if (!empty($news)): ?>
    <div class="col-md-6 col-sm-12">
      <hgroup class="main-title">
          <h2>Aktualności </br> neuroreha</h2>
          <h4>Co u nas</h4>
        </hgroup>
        <?php
          $post= $news[0];
          // setup_postdata( $recent[0] );
          get_template_part( 'templates/content', 'featured' );
          ?>
      </div>
    <?php endif ?>
  </div>
</div>