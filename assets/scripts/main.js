/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
var initialize;
initialize = function() {
        var e, t, a, n, i, s, o, l, r, d, g, u;
        a = new google.maps.DirectionsService, t = new google.maps.DirectionsRenderer, d = void 0, g = !1, s = new google.maps.LatLng(50.680294, 17.970784),
         u = [{
            featureType: "administrative",
            elementType: "all",
            stylers: [{
                visibility: "on"
            }, {
                saturation: -100
            }, {
                lightness: 20
            }]
        }, {
            featureType: "road",
            elementType: "all",
            stylers: [{
                visibility: "on"
            }, {
                saturation: -100
            }, {
                lightness: 40
            }]
        }, {
            featureType: "water",
            elementType: "all",
            stylers: [{
                visibility: "on"
            }, {
                saturation: -10
            }, {
                lightness: 30
            }]
        }, {
            featureType: "landscape.man_made",
            elementType: "all",
            stylers: [{
                visibility: "simplified"
            }, {
                saturation: -60
            }, {
                lightness: 10
            }]
        }, {
            featureType: "landscape.natural",
            elementType: "all",
            stylers: [{
                visibility: "simplified"
            }, {
                saturation: -60
            }, {
                lightness: 60
            }]
        }, {
            featureType: "poi",
            elementType: "all",
            stylers: [{
                visibility: "off"
            }, {
                saturation: -100
            }, {
                lightness: 60
            }]
        }, {
            featureType: "transit",
            elementType: "all",
            stylers: [{
                visibility: "off"
            }, {
                saturation: -100
            }, {
                lightness: 60
            }]
        }], l = {
            zoom: 15,
            center: s,
            styles: u
        }, e = "<h4>Neuroreha</h4><br>" + d, i = new google.maps.InfoWindow({
            content: e
        }), r = new google.maps.Marker({
            position: s,
            map: o,
            title: "Neuroreha",
            icon: n
        }), o = new google.maps.Map(document.getElementById("map"), l), navigator.geolocation && navigator.geolocation.getCurrentPosition(function(e) {
            var n;
            return d = new google.maps.LatLng(e.coords.latitude, e.coords.longitude), n = {
                origin: s,
                destination: d,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            }, t.setMap(o), a.route(n, function(e, a) {
                a === google.maps.DirectionsStatus.OK && t.setDirections(e)
            })
        }, function() {
            handleNoGeolocation(!0)
        }), google.maps.event.addListener(r, "click", function() {
            return i.open(o, r)
        })
    },
    function() {
        var e, t, a, n, i, s, o, l;
        if (t = void 0, e = void 0, s = void 0, i = void 0, o = void 0, l = function() {
                var e;
                for (e = this; - 1 === e.className.indexOf("nav-menu");) "li" === e.tagName.toLowerCase() && (-1 !== e.className.indexOf("focus") ? e.className = e.className.replace(" focus", "") : e.className += " focus"), e = e.parentElement
            }, t = document.getElementById("site-navigation"), t && (e = t.getElementsByTagName("button")[0], "undefined" != typeof e)) {
            if (s = t.getElementsByTagName("ul")[0], "undefined" == typeof s) return void(e.style.display = "none");
            for (s.setAttribute("aria-expanded", "false"), -1 === s.className.indexOf("nav-menu") && (s.className += " nav-menu"), e.onclick = function() {
                    -1 !== t.className.indexOf("toggled") ? (e.className = e.className.replace(" close", ""), t.className = t.className.replace(" toggled", ""), e.setAttribute("aria-expanded", "false"), s.setAttribute("aria-expanded", "false")) : (e.className += " close", t.className += " toggled", e.setAttribute("aria-expanded", "true"), s.setAttribute("aria-expanded", "true"))
                }, i = s.getElementsByTagName("a"), o = s.getElementsByTagName("ul"), a = 0, n = o.length; n > a;) o[a].parentNode.setAttribute("aria-haspopup", "true"), a++;
            for (a = 0, n = i.length; n > a;) i[a].addEventListener("focus", l, !0), i[a].addEventListener("blur", l, !0), a++
        }
    }(),
    function() {
        var e, t, a;
        a = navigator.userAgent.toLowerCase().indexOf("webkit") > -1, t = navigator.userAgent.toLowerCase().indexOf("opera") > -1, e = navigator.userAgent.toLowerCase().indexOf("msie") > -1, (a || t || e) && document.getElementById && window.addEventListener && window.addEventListener("hashchange", function() {
            var e, t;
            t = location.hash.substring(1), e = void 0, /^[A-z0-9_-]+$/.test(t) && (e = document.getElementById(t), e && (/^(?:a|select|input|button|textarea)$/i.test(e.tagName) || (e.tabIndex = -1), e.focus()))
        }, !1)
    }(), jQuery(document).ready(function(e) {
        return e("#map").length > 0 ? google.maps.event.addDomListener(window, "load", initialize) : void 0
    });
//# sourceMappingURL=app.js.map
(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  $('.testimonials blockquote .hide').each(function(index, el) {
    console.log($(el).height());
    $(el).attr('original-height', $(el).height() )
    if ( $(el).height() > 122 ) {
      $(el).height(122).siblings(".expand").show();
    }
  });
  $('.testimonials .expand').on('click', function() {
    var hide =  $(this).siblings('.hide');
    orgHeight = hide.attr('original-height');

    if ( orgHeight > 122  ) {
      if (hide.hasClass('opened')) hide.animate({height: orgHeight}).toggleClass('opened');
      else hide.animate({height: 122}).toggleClass('opened');
    }

  });
  // Load Events
  $(document).ready(UTIL.loadEvents);
  $('.carousel').slick({
    autoplay: true,
    autoplaySpeed : 4000,
    arrows : false,
    dots: false,
    speed: 2500,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
  });
  $('.testimonials').slick({
      prevArrow: '<div class="slick-prev"></div>',
      nextArrow: '<div class="slick-next"></div>'
    });


})(jQuery); // Fully reference jQuery after this point.
